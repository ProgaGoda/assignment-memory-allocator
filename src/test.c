#define HEAP_SIZE 1024*16
#define TEST_1_MALLOC_SIZE 1000
#define TEST_2_MALLOC_SIZE 5000
#define TEST_3_MALLOC_SIZE 4000
#define TEST_4_MALLOC_SIZE 10000


#include "test.h"

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <sys/mman.h>


void debug (const char *fmt, ...);


static struct block_header *get_block_header (void *data);

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd );
							   
static void* block_after( struct block_header const* block );




void test1 (struct block_header *first_block)
{
    debug("STARTING TEST 1\n");
    void *data = _malloc(TEST_1_MALLOC_SIZE);
	debug("TEST 1: NULL CHECK\n\n");
    if (!data)
    {	
		err("_malloc returned NULL");
    }
    debug_heap(stdout, first_block);
	debug("TEST 1: SIZE CHECK\n\n");
    if (first_block->capacity.bytes != TEST_1_MALLOC_SIZE)
    {
        err("_malloc returned wrong-sized block");
    }
    _free(data);
	debug("TEST 1 FINISHED SUCCESSFULLY.\n\n");

}

void test2 (struct block_header *first_block)
{
    debug("STARTING TEST 2\n");
    void *data1 = _malloc(TEST_2_MALLOC_SIZE);
    void *data2 = _malloc(TEST_2_MALLOC_SIZE);
	debug("TEST 2: NULL CHECK\n\n");
    if (data1 == NULL || data2 == NULL)
    {
		err("_malloc returned NULL");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = get_block_header(data1);
    struct block_header *data2_block = get_block_header(data2);
    if (data1_block->is_free == false || data2_block->is_free == true) {
		err("_free failed (wrong is_free value)");
	}
    _free(data2);
	debug("TEST 2 FINISHED SUCCESSFULLY.\n\n");

}

void test3 (struct block_header *first_block)
{
    debug("STARTING TEST 3\n");
    void *data1 = _malloc(TEST_3_MALLOC_SIZE);
    void *data2 = _malloc(TEST_3_MALLOC_SIZE);
    void *data3 = _malloc(TEST_3_MALLOC_SIZE);
	debug("TEST 3: NULL CHECK\n\n");
    if (data1 == NULL || data2 == NULL || data3 == NULL)
    {
        err("_malloc returned NULL");
    }
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_header = get_block_header(data1);
    struct block_header *data2_header = get_block_header(data2);
    struct block_header *data3_header = get_block_header(data3);
	debug("TEST 3: _free CHECK\n\n");
    if (data1_header->is_free == false || data2_header->is_free == false || data3_header->is_free == true)
		err("_free failed (wrong is_free value)");
    if (data1_header->capacity.bytes != 2 * TEST_3_MALLOC_SIZE + offsetof(struct block_header, contents))
        err("_free failed (wrong block size value)");
    _free(data3);
	debug_heap(stdout, first_block);
	debug("TEST 3 FINISHED SUCCESSFULLY.\n\n");

}

void test4 (struct block_header *first_block)
{
    debug("STARTING TEST 4:\n");
    void *data1 = _malloc(TEST_4_MALLOC_SIZE);
    void *data2 = _malloc(TEST_4_MALLOC_SIZE);
    void *data3 = _malloc(TEST_4_MALLOC_SIZE);
	debug("TEST 4: NULL CHECKS AFTER HEAP OVERFLOW\n");
    if (data1 == NULL || data2 == NULL || data3 == NULL)
    {
        err("_malloc returned null");
    }
    debug_heap(stdout, first_block);
    struct block_header *data1_header = get_block_header(data1);
    struct block_header *data2_header = get_block_header(data2);
	debug("TEST 4: ADJACENCY CHECK\n");
    if (!blocks_continuous(data1_header, data2_header))
    {
        err("ERROR: BLOCKS ARE NOT ADJACENT");
    }
	debug_heap(stdout, first_block);
    _free(data1);
    _free(data2);
    _free(data3);
    debug("TEST 4 FINISHED SUCCESSFULLY.\n\n");
}



void test()
{
    struct block_header *first_block = (struct block_header *) heap_init(HEAP_SIZE);
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
	debug("All tests are passed!\n");
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}

static struct block_header *get_block_header (void *data)
{
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}
